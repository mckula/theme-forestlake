<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package forest_lake
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="page-top">
				<div class="container">
					<div class="page-top-in">
						<h1>
							<span>
								<?php echo str_replace('County: ','',get_the_archive_title()); ?> County
							</span>
						</h1>
					</div>
				</div>
			</section>

			<section class="pgl-properties pgl-bg-grey">
				<div class="container">
					<div class="row">

			<?php query_posts($query_string . "&order=ASC"); 
			
			if ( have_posts() ) : ?>

			<?php /* Start the Loop */
			while ( have_posts() ) : the_post();
				$title = get_the_title();
				
			?>
				<div class="col-xs-12 col-sm-6 col-md-3 animation">
					<a href="<?php echo esc_url(get_permalink()); ?>">
						<div class="pgl-property">
							<div class="property-thumb-info">
								<div class="property-thumb-info-image">

									<?php $image = get_field('feature_image'); ?>

									<img src="<?php echo $image['url']; ?>" alt="<?php echo ' listing' . the_title(); ?>" class="img-responsive" />

									<span class="property-thumb-info-label">
									    <?php if(strlen(get_field('price')) >= 7): ?>
										<span class="label price"><small>$ <?php the_field('price'); ?></small></span>
										<?php else: ?>
										<span class="label price">$ <?php the_field('price'); ?></span>
										<?php endif; ?>
										
										
										<?php if(get_field('type') !== "Residential"): ?>
											<span class="label pull-right"><small><?php the_field('type'); ?></small></span>
										<?php endif; ?>
									</span>
								</div>
								<div class="property-thumb-info-content">
									<h3>
										<?php the_title(); ?>

										<?php if(get_field('area') && strlen($title) < 23 ): ?>
											<br/>(<?php the_field('area'); ?>)
										<?php else: ?>
											<br/>
										<?php endif; ?>
									</h3>
									<address>
										<strong>Zip:</strong> <?php the_field('zip'); ?> <br/>
										<?php if(get_field('lot')): ?>
										<strong>Lot:</strong> <?php the_field('lot'); ?> 
										<?php endif; ?>
									</address>
								</div>
								<div class="amenities clearfix">
									<ul>
										<li><strong>MLS #:</strong> <?php the_field('mls'); ?><br/></li>
										<?php if(get_field('type') !== "Multi/Family"): ?>
                                            <?php if(get_field('bedrooms')): ?>
                                                <li><i class="fa fa-bed"></i> <strong>Bed:</strong> <?php the_field('bedrooms'); ?></li>
                                            <?php endif; ?>
                                            
                                            <?php if(get_field('bathrooms')): ?>
                                                <li><i class="fa fa-tint"></i> <strong>Bath:</strong> <?php the_field('bathrooms'); ?></li>
                                            <?php endif; ?>
                                        <?php endif; ?>  
                                           
                                        <?php if(get_field('type') == "Multi/Family"): ?>
                                            <?php if(get_field('style')): ?>
                                            <li><strong>Style:</strong> <?php the_field('style'); ?></li>
                                            
                                            <?php else: ?>

                                            <?php if(get_field('bedrooms')): ?>
                                                <li><i class="fa fa-bed"></i> <strong>Bed:</strong> <?php the_field('bedrooms'); ?></li>
                                            <?php endif; ?>

                                            <?php if(get_field('bathrooms')): ?>
                                                <li><i class="fa fa-tint"></i> <strong>Bath:</strong> <?php the_field('bathrooms'); ?></li>
                                            <?php endif; ?>
                                            
                                            <?php endif; ?>
                                            
                                        <?php endif; ?>
                                           
                                            <?php if(get_field('type') == "Farms/Acreage/Land"): ?>
                                                <li><strong>Acres:</strong> <?php the_field('acres'); ?></li>
                                            <?php endif; ?></li>
									</ul>
									
								</div>
							</div>
						</div>
					</a>
				</div>



		<?php

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
		</div>
		</div>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
