<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package forest_lake
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="pgl-properties">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
			<?php 
				$terms = get_terms( 'counties', $args ); 
		
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ 
					echo "<ul>"; 

				foreach ( $terms as $term ) { 
					echo "<li class='county'><a href='" . get_term_link($term) . "'>" . $term->name . ' County</a></li>'; } echo '</ul>'; 
				}
			?>
						</div>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
