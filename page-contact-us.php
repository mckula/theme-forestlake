<?php get_header() ?>

<!-- Begin Main -->
		<div role="main" class="main pgl-bg-grey">
			<!-- Begin page top -->
			<section class="page-top">
				<div class="container">
					<div class="page-top-in">
						<h1><span>Contact Us</span></h1>
					</div>
				</div>
			</section>
			<!-- End page top -->
			
			<!-- Begin content with sidebar -->
			<div class="container">
				<div class="row">
					<div class="col-md-9 content">
						
						<div class="contact">
							<div class="mapp" style="margin-bottom:40px;">
							<div class="overlay visible-lg" onClick="style.pointerEvents='none'"></div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12198.049915540085!2d-79.5510618!3d40.1531421!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4caa64c7cc572ca9!2sForest+Lake+Group!5e0!3m2!1sen!2sus!4v1450295866666" width="847" height="305" frameborder="0" style="border:0" allowfullscreen></iframe></div>
							<div class="row">
								<div class="col-sm-6">
									<strong>Address</strong>
									<address>10 Morewood St # A, Mt Pleasant, PA 15666</address>
								</div>
								<div class="col-sm-6">
									<address>
										<strong>Phone.</strong> 724-542-0350<br>
										<strong>Fax.</strong> 724-542-0353<br>
										<strong>Toll-Free.</strong> 1-866-950-5343
									</address>
								</div>
							</div>
							<hr>
							<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 8, 'title' => false, 'description' => false ) ); ?>
						</div>
						
					</div>
					<div class="col-md-3 sidebar">
						
						<!-- Begin Our Agents -->
						<aside class="block pgl-agents pgl-bg-light">
								<div class="pgl-agent-item">
									<div class="pgl-agent-info">
										<h4>Chuck Vorum</h4>
										<address>
											<i class="fa fa-phone"></i> Office: 724-542-0350<br>
											<i class="fa fa-phone-square"></i> Toll-Free: 1-866-950-5343<br>
											<i class="fa fa-fax"></i> Fax: 724-542-0353<br>
											<i class="fa fa-envelope-o"></i> Email: <a class="gr" href="mailto:forestre@cvzoom.net">forestre@cvzoom.net</a>
										</address>
									</div>	
								</div>
						</aside>
						<!-- End Our Agents -->

						<!-- Begin Our Agents -->
						<aside class="block pgl-agents pgl-bg-light">
								<div class="pgl-agent-item">
									<div class="pgl-agent-info">
										<h4>Jeffrey Walker</h4>
										<address>
											<i class="fa fa-mobile"></i> Cell: 814-233-6500<br>
											<i class="fa fa-phone"></i> Office: 724-542-0350<br>
											<i class="fa fa-phone-square"></i> Toll-Free: 1-866-950-5343<br>
											<i class="fa fa-fax"></i> Fax: 724-542-0353<br>
											<i class="fa fa-envelope-o"></i> Email: <a class="gr" href="mailto:rangerwalker33@hotmail.com" style="font-size:smaller;">rangerwalker33@hotmail.com </a>
										</address>
									</div>	
								</div>
						</aside>
						<!-- End Our Agents -->
						
						
						
					</div>
				</div>	
			</div>
			<!-- End content with sidebar -->
			
		</div>
		<!-- End Main -->

<?php get_footer() ?>