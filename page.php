<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package forest_lake
 */

get_header(); ?>

<!-- Begin Main -->
	<div role="main" class="main">
		<!-- Begin Advanced Search -->
		<section class="pgl-advanced-search pgl-bg-light">
			<div class="container">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
						<div class="col-xs-6 col-sm-3"><label class="sr-only" for="property-status">Address</label>
							<div class="form-group">
								<input type="search" class="form-control" placeholder="Address, Zip, MLS #, etc." required="" aria-required="true" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for anything', 'label' ) ?>" />
							</div>
						</div>	
						<div class="col-xs-6 col-sm-3">
							<div class="form-group">
								<input type="submit" class="btn btn-block btn-primary" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
							</div>
						</div>	
					</div>
				</form>

			</div>
		</section>
		<!-- End Advanced Search -->
		
		<section class="pgl-featured pgl-bg-grey">
			<div class="container">
			<h1>Featured Properties</h1>
				<div class="row">
					
							<?php 
								$args = array(
									'numberposts'	=> -1,
									'post_type'		=> 'properties',
									'meta_query' => array(
    									array(
        									'key' => 'show_on_home_page',
        									'value' => '1'
    										)
									)
								);

								$the_query = new WP_Query( $args );
								$count = 1;

							?>
								<?php if( $the_query->have_posts() ): ?>
									
									<?php while( $the_query->have_posts() ) : $the_query->the_post(); 

										$image = get_field('feature_image');
										$url = $image['url'];
										$alt = $image['alt'];

									?>

									<?php if( $count == 1 ): ?>
										<div class="col-md-6 animation">
											<a href="<?php echo esc_url(get_permalink()); ?>">
												<div class="pgl-property featured-item">
													<div class="property-thumb-info">
														<div class="property-thumb-info-image">
															<img src="<?php echo $url ?>" alt="<?php the_title(); ?>" class="img-responsive" />
														</div>
														<div class="property-thumb-info-content">
															<h3 class="feature">
																	<?php the_title(); ?>
																	(<?php echo the_field('area'); ?>)
															</h3>
															<p>Price: $<?php echo the_field('price')?></p>
															<?php 

							   									$taxonomy_objects = get_the_terms( $post->ID, 'counties' );
																$slug = $taxonomy_objects[0]->slug;
															    $name = $taxonomy_objects[0]->name;

															?>

															<p><?php echo $name . " County" ?></p>
														</div>
													</div>
												</div>
											</a>
										</div>
										<?php $count++; ?>
									
									<?php else: ?>

										<div class="col-xs-6 col-md-3 animation">
											<a href="<?php echo esc_url(get_permalink()); ?>">
												<div class="pgl-property featured-item">
													<div class="property-thumb-info">
														<div class="property-thumb-info-image">
															<img src="<?php echo $url ?>" alt="<?php the_title(); ?>" class="img-responsive" />
														</div>
														<div class="property-thumb-info-content">
															<h3 class="feature">
																		<?php the_title(); ?>
																		(<?php echo the_field('area'); ?>)
															</h3>
															<p>Price: $<?php echo the_field('price')?></p>
															<?php 

							   									$taxonomy_objects = get_the_terms( $post->ID, 'counties' );
																$slug = $taxonomy_objects[0]->slug;
															    $name = $taxonomy_objects[0]->name;

															?>

															<p><?php echo $name . " County" ?></p>														</div>
													</div>
												</div>
											</a>
										</div>

									<?php endif; ?>
									
									<?php endwhile; ?>
									
								<?php endif; ?>

							<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
					
				</div>
			</div>
		</section>

		<!-- Begin Agents -->
		<section class="pgl-agents">
			<div class="container">
				<h2>Our Agents</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="pgl-agent-item pgl-bg-light">
							<div class="row pgl-midnarrow-row">
								<div class="col-xs-12">
									<div class="pgl-agent-info">
										<h4>Chuck Vorum</h4>
										<p>Forest Lake Real Estate Group <br/>10 Morewood Street, Mt. Pleasant, PA 15666</p>
										<address>
											<i class="fa fa-map-marker"></i> Office : 724-542-0350<br>
											<i class="fa fa-phone"></i> Mobile : 1-866-950-5343<br>
											<i class="fa fa-fax"></i> Fax : 724-542-0353<br>
											<i class="fa fa-envelope-o"></i> Mail: <a href="mailto:forestre@cvzoom.net">forestre@cvzoom.net</a>
										</address>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="pgl-agent-item pgl-bg-light">
							<div class="row pgl-midnarrow-row">
								<div class="col-xs-12">
									<div class="pgl-agent-info">
										<h4>Jeffrey Walker</h4>
										<p>Forest Lake Real Estate Group <br/>
											10 Morewood Street, Mt. Pleasant, PA 15666</p>
										<address>
											<i class="fa fa-map-marker"></i> Office : 724-542-0350<br>
											<i class="fa fa-phone"></i> Mobile : 814-233-6500<br>
											<i class="fa fa-fax"></i> Fax : 724-542-0353<br>
											<i class="fa fa-envelope-o"></i> Mail: <a href="mailto:rangerwalker33@hotmail.com">rangerwalker33@hotmail.com </a>
										</address>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr class="top-tall">
			</div>
		</section>
		<!-- End Agents -->

		
	</div>
	<!-- End Main -->
<?php
get_sidebar();
get_footer();
