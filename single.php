<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package forest_lake
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="pgl-properties pgl-bg-grey">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php 

							   $taxonomy_objects = get_the_terms( $post->ID, 'counties' );
							   $slug = $taxonomy_objects[0]->slug;
							   $name = $taxonomy_objects[0]->name;

							?>

							<a href="<?php echo site_url() . '/county/' . $slug ?>" class="back"><i class="fa fa-chevron-left"></i> Back to <?php echo $name ?> County</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-9 content">
							<section class="pgl-pro-detail pgl-bg-light">

								<?php $images = get_field('images');

								if( $images ): ?>
								<div id="slider" class="flexslider">
									<ul class="slides">
									    <?php $image_count = 1; ?>
										<?php foreach( $images as $image ): ?>
										<li>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo get_the_title() . ' ' . $image_count;  ?>" class="img-responsive" />
										</li>
										<?php $image_count++; ?>
										<?php endforeach; ?>
									</ul>
								</div>
								<div id="carousel" class="flexslider">
									<ul class="slides">
										<?php $image_count = 1; ?>
										<?php foreach( $images as $image ): ?>
											<li><img src="<?php echo $image['url']; ?>" alt="<?php echo get_the_title() . ' ' . $image_count . ' tmb'; ?>" class="img-responsive" /></li>
											<?php $image_count++; ?>
										<?php endforeach; ?>
									</ul>
								</div>
								<?php endif; ?>

								<div class="pgl-detail">
									<div class="row">

										<div class="col-sm-12">
											<h1 class="text-left lower">
												<?php if(get_field('area')) : ?>
													<?php echo trim(get_the_title() . ' <small>(' .  get_field('area') . ')</small>'); ?>
													<br/><small>MLS: <?php echo the_field('mls'); ?></small>
												<?php endif; ?>

												<?php if(!get_field('area')) : ?>
													<?php echo trim(the_title()); ?>
													<br/><small>MLS: <?php echo the_field('mls'); ?></small>
												<?php endif; ?>

											</h1>
											<?php if(get_field('description')): ?>
												<p>
													<strong>Description:</strong><br/>
													<?php echo the_field('description'); ?>
												</p>
											<?php endif; ?>
											<?php if(get_field('directions')): ?>
												<p>
													<strong>Directions:</strong><br/>
													<?php echo the_field('directions'); ?>
												</p>
											<?php endif; ?>	
										</div>
									</div>
								</div>
							</section>
						</div>

						<div class="col-md-3 sidebar">

								<ul class="list-unstyled amenities amenities-detail">
                                        <li><strong>Type:</strong> <span class="pull-right"><?php echo the_field('type') ?></span></li>
										<li><strong>Price:</strong> <span class="pull-right">$ <?php echo the_field('price') ?></span></li>
									<?php if(get_field('taxes')): ?>
										<li><strong>Taxes:</strong> <span class="pull-right">$ <?php echo the_field('taxes') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('style')): ?>
										<li><strong>Style:</strong> <span class="pull-right"><?php echo the_field('style') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('zip')): ?>
										<li><strong>Zip:</strong> <span class="pull-right"><?php echo the_field('zip') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('bedrooms')): ?>
										<li><strong>Bedrooms:</strong> <span class="pull-right"><?php echo the_field('bedrooms') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('bathrooms')): ?>
										<li><strong>Bathrooms:</strong> <span class="pull-right"><?php echo the_field('bathrooms') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('acres')): ?>
										<li><strong>Acres:</strong> <span class="pull-right"><?php echo the_field('acres') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('lot')): ?>
										<li><strong>Lot:</strong> <span class="pull-right"><?php echo the_field('lot') ?></span></li>
									<?php endif; ?>

									<?php if(get_field('buildings')): ?>
										<li><strong>Buildings:</strong> <span class="pull-right"><?php echo the_field('buildings') ?></span></li>
									<?php endif; ?>	

									<?php if(get_field('terrain')): ?>
										<li><strong>Terrain:</strong> <span class="pull-right"><?php echo the_field('terrain') ?></span></li>
									<?php endif; ?>	

									<?php if(get_field('frontage')): ?>
										<li><strong>Frontage:</strong> <span class="pull-right"><?php echo the_field('frontage') ?></span></li>
									<?php endif; ?>	

									<?php if(get_field('depth')): ?>
										<li><strong>Depth:</strong> <span class="pull-right"><?php echo the_field('depth') ?></span></li>
									<?php endif; ?>	

									<?php if(get_field('zone')): ?>
										<li><strong>Zone:</strong> <span class="pull-right"><?php echo the_field('zone') ?></span></li>
									<?php endif; ?>												
								</ul>
										
								<!-- Begin Advanced Search -->
								<aside class="block pgl-advanced-search pgl-bg-light">
									<div class="tab-detail">
											<div class="panel-group" >
											    <?php if( have_rows('rooms') ): ?>
												<div class="panel panel-default pgl-panel">
												
													<div class="panel-heading">
														<h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Additional Details</a> </h4>
													</div>
													<div id="collapseOne" class="panel-collapse collapse in">
														<div class="panel-body">

															<ul class="slides">
																<li><strong>Rooms:</strong></li>
															<?php while( have_rows('rooms') ): the_row(); 
                                                                
																$room = get_sub_field('room');
																$size = get_sub_field('size');
																?>
																<li class="slide">
																    <?php echo $room; ?> <span style="float:right;"><?php echo $size ?></span>
																</li>
															<?php endwhile; ?>
															</ul>
								
														</div>
													</div>
											    <?php endif; ?>
												</div>
																								
												<div class="panel panel-default pgl-panel">
													<a href="<?php echo site_url('contact-us'); ?>">
														<span class="btn btn-block btn-primary">Contact Us</span>
													</a>
												</div>
											</div>
										</div>
								</aside>
								<!-- End Advanced Search -->
						</div>
						
					</div>

				</div>
			</section>
		
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
