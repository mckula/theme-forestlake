window.onload = function() {
 
 	$count = 0;

    $(".navbar-toggle").click(function(){
    	if($count % 2 == 0) {
    		$(".flex-prev").hide();
    		$(".flex-next").hide();
    	}
    	else {
    		$(".flex-next").show();
    		$(".flex-prev").show();
    	}
    	$count++;
	});
 
};