<div class="row">
							<div class="col-xs-6 col-md-3 animation">
								<div class="pgl-property">
									<div class="property-thumb-info">
										<div class="property-thumb-info-image">
											<img alt="" class="img-responsive" src="images/properties/property-1.jpg">
											<span class="property-thumb-info-label">
												<span class="label price">$358,000</span>
												<span class="label forrent">Rent</span>
											</span>
										</div>
										<div class="property-thumb-info-content">
											<h3><a href="property-detail.html">Poolside character home on a wide 422sqm</a></h3>
											<address>Ferris Park, Jersey City Land in Sales</address>
										</div>
										<div class="amenities clearfix">
											<ul class="pull-left">
												<li><strong>Area:</strong> 450<sup>m2</sup></li>
											</ul>
											<ul class="pull-right">
												<li><i class="icons icon-bedroom"></i> 3</li>
												<li><i class="icons icon-bathroom"></i> 2</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-3 animation">
								<div class="pgl-property">
									<div class="property-thumb-info">
										<div class="property-thumb-info-image">
											<img alt="" class="img-responsive" src="images/properties/property-2.jpg">
											<span class="property-thumb-info-label">
												<span class="label price">$358,000</span>
											</span>
										</div>
										<div class="property-thumb-info-content">
											<h3><a href="property-detail.html">Presidential Parcel Frames Command Views</a></h3>
											<address>Ferris Park, Jersey City Land in Sales</address>
										</div>
										<div class="amenities clearfix">
											<ul class="pull-left">
												<li><strong>Area:</strong> 450<sup>m2</sup></li>
											</ul>
											<ul class="pull-right">
												<li><i class="icons icon-bedroom"></i> 3</li>
												<li><i class="icons icon-bathroom"></i> 2</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-3 animation">
								<div class="pgl-property">
									<div class="property-thumb-info">
										<div class="property-thumb-info-image">
											<img alt="" class="img-responsive" src="images/properties/property-3.jpg">
											<span class="property-thumb-info-label">
												<span class="label price">$358,000</span>
												<span class="label forsold">Sold</span>
											</span>
										</div>
										<div class="property-thumb-info-content">
											<h3><a href="property-detail.html">Californian Class, Grand Family Proportions</a></h3>
											<address>Ferris Park, Jersey City Land in Sales</address>
										</div>
										<div class="amenities clearfix">
											<ul class="pull-left">
												<li><strong>Area:</strong> 450<sup>m2</sup></li>
											</ul>
											<ul class="pull-right">
												<li><i class="icons icon-bedroom"></i> 3</li>
												<li><i class="icons icon-bathroom"></i> 2</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-3 animation">
								<div class="pgl-property">
									<div class="property-thumb-info">
										<div class="property-thumb-info-image">
											<img alt="" class="img-responsive" src="images/properties/property-3.jpg">
											<span class="property-thumb-info-label">
												<span class="label price">$358,000</span>
												<span class="label forsold">Sold</span>
											</span>
										</div>
										<div class="property-thumb-info-content">
											<h3><a href="property-detail.html">Californian Class, Grand Family Proportions</a></h3>
											<address>Ferris Park, Jersey City Land in Sales</address>
										</div>
										<div class="amenities clearfix">
											<ul class="pull-left">
												<li><strong>Area:</strong> 450<sup>m2</sup></li>
											</ul>
											<ul class="pull-right">
												<li><i class="icons icon-bedroom"></i> 3</li>
												<li><i class="icons icon-bathroom"></i> 2</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>