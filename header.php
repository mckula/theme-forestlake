<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package forest_lake
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="google-site-verification" content="bs8zF1BuClk81vVHtd1Wc8ftEGtvQ1uZoPjuTxx0wu4" />

	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="icon" href="<?php echo get_template_directory_uri() . '/favicon.png'?>" />

	<!-- Bootstrap -->
	<link href="<?php echo get_template_directory_uri() . "/assets/bootstrap/bootstrap.min.css"?>" rel="stylesheet">

	<!-- Libs CSS -->
	<link href="<?php echo get_template_directory_uri() . "/assets/font-awesome.min.css"?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/owl-carousel/owl.carousel.css" ?>" media="screen">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/owl-carousel/owl.theme.css" ?>" media="screen">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/flexslider/flexslider.css" ?>" media="screen">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() . "/assets/chosen/chosen.css" ?>" media="screen">

	<!-- Theme -->
	<link href="<?php echo get_template_directory_uri() . "/assets/theme-animate.css" ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri() . "/assets/theme-elements.css" ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri() . "/assets/theme-blog.css" ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri() . "/assets/theme-map.css" ?>" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri() . "/assets/theme.css" ?>" rel="stylesheet">
	
	<!-- Theme Responsive-->
	<link href="<?php echo get_template_directory_uri() . "/assets/theme-responsive.css"?>" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header>
			<div id="top">
				<div class="container">
					<p class="pull-left text-note hidden-xs"><i class="fa fa-phone"></i> Call Now! 724-542-0350</p>
				</div>
			</div>
			<nav class="navbar navbar-default pgl-navbar-main" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<a class="logo" href="<?php echo site_url(); ?>">
						<img src="<?php echo get_template_directory_uri() . "/img/logo.png"?>" alt="Forest Lake Group LLC - Mount Pleasant PA"></a> </div>
					
					<div class="navbar-collapse collapse width">
						<ul class="nav navbar-nav pull-right">
							<li><a href="<?php echo site_url(); ?>" class="dropdown-toggle">Home</a>
							</li>
							<li class="dropdown"><a class="dropdown-toggle">Properties</a>
									<?php 
									
									$terms = get_terms( 'counties'); 

									if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ 
										echo "<ul class='dropdown-menu'>"; 

									foreach ( $terms as $term ) { 
										echo "<li><a href='" . get_term_link($term) . "'>" . $term->name . ' County</a></li>'; } echo '</ul>'; 
									}
									?>
							</li>
							<li><a href="<?php echo site_url('contact-us'); ?>" class="dropdown-toggle">Contact Us</a>
							</li>
						</ul>
					</div><!--/.nav-collapse --> 
				</div><!--/.container-fluid --> 
			</nav>
		</header>
	<div id="content" class="site-content">
