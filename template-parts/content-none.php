<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package forest_lake
 */

?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="pgl-properties pgl-bg-grey">
			<div class="container">
				<div class="row">
					<section class="no-results not-found">
						<header class="page-header">
							<h1 class="page-title"><?php esc_html_e( 'Search for an address', 'forest-lake' ); ?></h1>
						</header><!-- .page-header -->

						<div class="page-content">
							<?php
							if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

								<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'forest-lake' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

							<?php elseif ( is_search() ) : ?>

								<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords or click a link below.', 'forest-lake' ); ?></p>
								<?php
									get_search_form();

							else : ?>

								<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'forest-lake' ); ?></p>
								<?php
									get_search_form();

							endif; ?>
							
							<div class="row">
                                <div class="col-md-12">
                                    <?php 
									
									$terms = get_terms( 'counties'); 

									if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ 
										echo ""; 

									foreach ( $terms as $term ) { 
										echo "<div style='margin:5px;font-weight:bold;'><a href='" . get_term_link($term) . "'>" . $term->name . ' County</a></div>'; } 
									}
									?>
                                </div>
                            </div>
						</div><!-- .page-content -->
					</section><!-- .no-results -->
				</div>
			</div>
		</section>
	</main>
</div>
