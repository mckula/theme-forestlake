<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package forest_lake
 */

?>

<?php if (!empty($_GET["mls"]) && $_GET["mls"] == get_field('mls') ) { ?>

<div class="container">asdf
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="pgl-property animation">
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<a href="<?php echo esc_url(get_permalink()); ?>">
						<div class="property-thumb-info-image">

							<?php $image = get_field('feature_image'); ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>" class="img-responsive" />
							
							<span class="property-thumb-info-label">
								<span class="label price">$a <?php the_field('price'); ?></span>
							</span>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-8">
					<div class="property-thumb-info">
						<div class="property-thumb-info-content">
							<h2>
								<a href="<?php echo esc_url(get_permalink()); ?>">
									<?php the_title(); ?>
									<?php if(get_field('area')): ?>
										<small>(<?php the_field('area'); ?>)</small>
									<?php endif; ?>
								</a>
							</h2>
							
							<p><strong>Description:</strong> <?php the_field('description'); ?></p>
							<p><strong>Directions:</strong> <?php the_field('directions'); ?></p>
						</div>
						<div class="amenities clearfix">
							<ul class="pull-left">
								<?php if(get_field('zip')): ?>
									<li><strong>Zip:</strong> <?php the_field('zip'); ?></li>
								<?php endif; ?>
								<?php if(get_field('lot')): ?>
									<li><strong>Lot:</strong> <?php the_field('lot'); ?></li>
								<?php endif; ?>
								<?php if(get_field('acres')): ?>
									<li><strong>Acres:</strong> <?php the_field('acres'); ?></li>
								<?php endif; ?>
								<?php if(get_field('frontage')): ?>
									<li><strong>Frontage:</strong> <?php the_field('frontage'); ?></li>
								<?php endif; ?>
							</ul>
							<ul class="pull-right">
								<?php if(get_field('bedrooms')): ?>
									<li><i class="fa fa-bed"></i> <strong>Bed:</strong> <?php the_field('bedrooms'); ?></li>
								<?php endif; ?>
								<?php if(get_field('bathrooms')): ?>
									<li><i class="fa fa-tint"></i> <strong>Bath:</strong> <?php the_field('bathrooms'); ?></li>
								<?php endif; ?>
								<?php if(get_field('depth')): ?>
									<li><strong>Depth:</strong> <?php the_field('depth'); ?></li>
								<?php endif; ?>
								<?php if(get_field('zone')): ?>
									<li><strong>Zone:</strong> <?php the_field('zone'); ?></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php forest_lake_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

		<footer class="entry-footer">
			<?php forest_lake_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->
</div>  
	
<?php 
} 

else  {  ?>
<div class="container">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="pgl-property animation">
			<div class="row">
				<div class="col-sm-6 col-md-4">
					<a href="<?php echo esc_url(get_permalink()); ?>">
						<div class="property-thumb-info-image">

							<?php $image = get_field('feature_image'); ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php the_title(); ?>" class="img-responsive" />
							
							<span class="property-thumb-info-label">
								<span class="label price">$ <?php the_field('price'); ?></span>
								<?php if(get_field('type') !== "Residential"): ?>
								    <span class="label pull-right"><small><?php the_field('type'); ?></small></span>
								<?php endif; ?>
							</span>
						</div>
					</a>
				</div>
				<div class="col-sm-6 col-md-8">
					<div class="property-thumb-info">
						<div class="property-thumb-info-content">
							<h2>
								<a href="<?php echo esc_url(get_permalink()); ?>">
									<?php the_title(); ?>
									<?php if(get_field('area')): ?>
										<small>(<?php the_field('area'); ?>)</small>
									<?php endif; ?>
								</a>
							</h2>
							
							<p><strong>Description:</strong> <?php the_field('description'); ?></p>
							<p><strong>Directions:</strong> <?php the_field('directions'); ?></p>
						</div>
						<div class="amenities clearfix">
							<ul class="pull-left">
								<?php if(get_field('zip')): ?>
									<li><strong>Zip:</strong> <?php the_field('zip'); ?></li>
								<?php endif; ?>
								<?php if(get_field('lot')): ?>
									<li><strong>Lot:</strong> <?php the_field('lot'); ?></li>
								<?php endif; ?>
								<?php if(get_field('acres')): ?>
									<li><strong>Acres:</strong> <?php the_field('acres'); ?></li>
								<?php endif; ?>
								<?php if(get_field('frontage')): ?>
									<li><strong>Frontage:</strong> <?php the_field('frontage'); ?></li>
								<?php endif; ?>
							</ul>
							<ul class="pull-right">
								<?php if(get_field('bedrooms')): ?>
									<li><i class="fa fa-bed"></i> <strong>Bed:</strong> <?php the_field('bedrooms'); ?></li>
								<?php endif; ?>
								<?php if(get_field('bathrooms')): ?>
									<li><i class="fa fa-tint"></i> <strong>Bath:</strong> <?php the_field('bathrooms'); ?></li>
								<?php endif; ?>
								<?php if(get_field('depth')): ?>
									<li><strong>Depth:</strong> <?php the_field('depth'); ?></li>
								<?php endif; ?>
								<?php if(get_field('zone')): ?>
									<li><strong>Zone:</strong> <?php the_field('zone'); ?></li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php forest_lake_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

		<footer class="entry-footer">
			<?php forest_lake_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->
</div> 

<?php } ?>