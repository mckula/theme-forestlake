<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package forest_lake
 */

?>

	</div><!-- #content -->

	<!-- Begin footer -->
		<footer class="pgl-footer">
			<div class="container">
				<div class="pgl-upper-foot">
					<div class="row">
						<div class="col-sm-4">
							<h2>Contact detail</h2>
							<address>
								<i class="fa fa-phone"></i> Office : 724-542-0350<br>
								<i class="fa fa-phone-square"></i> Toll-Free : 1-866-950-5343<br>
								<i class="fa fa-fax"></i> Fax : 724-542-0353<br>
								<i class="fa fa-envelope-o"></i> Mail: <a href="mailto:forestre@cvzoom.net">forestre@cvzoom.net</a>
							</address>
						</div>
						<div class="col-sm-2">
							<h2>Links</h2>
							<ul class="list-unstyled">
								<li><a href="http://arhauctioneer.com/">Aaron Howard Auctioneer</a></li>
							</ul>
						</div>
						<div class="col-sm-2">
							<h2>Pages</h2>
							<ul class="list-unstyled">
								<li><a href="<?php echo site_url(); ?>">Home</a></li>
								<li><a href="<?php echo site_url('properties'); ?>">Properties</a></li>
								<li><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
							</ul>
						</div>
						<div class="col-sm-4">
							<img id="img-res" src="<?php echo get_template_directory_uri() . "/img/resnet.jpg" ?>" alt="Resnet"/>
						</div>
					</div>
				</div>
				<div class="pgl-copyrights">
					<p>Copyright © <?php echo date("Y"); ?></p>
				</div>
			</div>
		</footer>
		<!-- End footer -->
			
	</div>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo get_template_directory_uri() . "/assets/jquery.min.js"?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/js/custom.js"?>"></script> 
	<!-- Include all compiled plugins (below), or include individual files as needed --> 
	<script src="<?php echo get_template_directory_uri() . "/assets/bootstrap/bootstrap.min.js" ?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/owl-carousel/owl.carousel.js" ?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/flexslider/jquery.flexslider-min.js" ?>"</script>
	<script src="<?php echo get_template_directory_uri() . "/assets/chosen/chosen.jquery.min.js" ?>"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/gmap/gmap3.infobox.min.js" ?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/masonry/imagesloaded.pkgd.min.js" ?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/masonry/masonry.pkgd.min.js" ?>"></script>
	
	<!-- Theme Initializer -->
	<script src="<?php echo get_template_directory_uri() . "/assets/js/theme.plugins.js" ?>"></script>
	<script src="<?php echo get_template_directory_uri() . "/assets/js/theme.js" ?>"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-2959404-26', 'auto');
	  ga('send', 'pageview');
	</script>

<?php wp_footer(); ?>

</body>
</html>
