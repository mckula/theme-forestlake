<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package forest_lake
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <section class="pgl-properties pgl-bg-grey">
                <div class="container">
                    <div class="row">
                        <section class="error-404 not-found">
                            <header class="page-header">
                                <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'forest-lake' ); ?></h1>
                            </header>
                            <!-- .page-header -->

                            <div class="page-content">
                                <p>
                                    <?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'forest-lake' ); ?>
                                </p>

                                <?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( forest_lake_categorized_blog() ) :
					?>

                                    <div class="widget widget_categories">
                                        <h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'forest-lake' ); ?></h2>
                                        <ul>
                                            <?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
                                        </ul>
                                    </div>
                                    <!-- .widget -->

                                    <?php
						endif;
					?>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <?php 
									
									$terms = get_terms( 'counties'); 

									if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ 
										echo ""; 

									foreach ( $terms as $term ) { 
										echo "<div style='margin:5px;font-weight:bold;'><a href='" . get_term_link($term) . "'>" . $term->name . ' County</a></div>'; } 
									}
									?>
                                </div>
                            </div>
                            </div>

                            
                            <!-- .page-content -->
                        </section>
                        <!-- .error-404 -->

                    </div>
                </div>

            </section>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_footer();