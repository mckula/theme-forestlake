<?php
/**
 * forest lake functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package forest_lake
 */

if ( ! function_exists( 'forest_lake_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function forest_lake_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on forest lake, use a find and replace
	 * to change 'forest-lake' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'forest-lake', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'forest-lake' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'forest_lake_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'forest_lake_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function forest_lake_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'forest_lake_content_width', 640 );
}
add_action( 'after_setup_theme', 'forest_lake_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function forest_lake_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'forest-lake' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'forest_lake_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function forest_lake_scripts() {
	wp_enqueue_style( 'forest-lake-style', get_stylesheet_uri() );

	wp_enqueue_script( 'forest-lake-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'forest-lake-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'forest_lake_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Our custom post type function
function create_posttype() {

	register_post_type( 'properties',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Properties' ),
				'singular_name' 	  => __( 'Property' ),
				'all_items'           => __( 'All Properties', 'forestlake' ),
				'view_item'           => __( 'View Property', 'forestlake' ),
				'add_new_item'        => __( 'Add New Property', 'forestlake' ),
				'add_new'             => __( 'Add New Property', 'forestlake' ),
				'edit_item'           => __( 'Edit Property', 'forestlake' ),
				'update_item'         => __( 'Update Property', 'forestlake' ),
				'search_items'        => __( 'Search Property', 'forestlake' )
			),
			'public' => true,
			'menu_icon' => 'dashicons-admin-home',
			'has_archive' => false,
			'rewrite' => array('slug' => 'properties'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


//hook into the init action and call create_counties_nonhierarchical_taxonomy when it fires

add_action( 'init', 'create_counties_nonhierarchical_taxonomy', 0 );

function create_counties_nonhierarchical_taxonomy() {

// Labels part for the GUI

  $labels = array(
    'name' => _x( 'Counties', 'taxonomy general name' ),
    'singular_name' => _x( 'County', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Counties' ),
    'popular_items' => __( 'Popular Counties' ),
    'all_items' => __( 'All Counties' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit County' ), 
    'update_item' => __( 'Update County' ),
    'add_new_item' => __( 'Add New County' ),
    'new_item_name' => __( 'New County Name' ),
    'separate_items_with_commas' => __( 'Separate counties with commas' ),
    'add_or_remove_items' => __( 'Add or remove counties' ),
    'choose_from_most_used' => __( 'Choose from the most used counties' ),
    'menu_name' => __( 'Counties' ),
  ); 

// Now register the non-hierarchical taxonomy like tag

  register_taxonomy('counties','properties',array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'county' ),
  ));
}

